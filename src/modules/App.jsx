import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import Home from '../pages/Home'
// Amplify configuration
import Amplify from 'aws-amplify'
import aws_exports from '../aws-exports'
import { Auth } from 'aws-amplify'
// Material-UI
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles'
import { loadCSS } from 'fg-loadcss/src/loadCSS'

require('dotenv').config()

Amplify.configure(aws_exports)
Amplify.configure({
  API: {
    endpoints: [
      {
        name: 'badges',
        endpoint: process.env.REACT_APP_BADGE_API_GW_URL
      }
    ]
  }
})
const theme = createMuiTheme({
  typography: {
    useNextVariants: true
  },
  palette: {
    primary: {
      main: '#4c65aa',
      light: 'rgba(166,64,78,0.8)'
    },
    secondary: { main: '#D96C46' },
    text: {
      primary: 'rgba(255, 255, 255, 0.8)',
      secondary: '#AD5051'
    }
  },
  status: {
    danger: 'red'
  }
})
class App extends Component {
  constructor (props) {
    super(props)
    this.state = {
      isAuthenticated: false,
      isAuthenticating: true
    }
  }
  async componentDidMount () {
    try {
      loadCSS('https://use.fontawesome.com/releases/v5.1.0/css/all.css', document.querySelector('#insertion-point-jss'))
      await Auth.currentSession()
      this.userHasAuthenticated(true)
    } catch (e) {
      if (e !== 'No current user') {
        console.error(e)
      }
    }
    this.setState({ isAuthenticating: false })
  }
  userHasAuthenticated = (authenticated) => {
    this.setState({ isAuthenticated: authenticated })
  }
  render () {
    const authProps = {
      isAuthenticated: this.state.isAuthenticated,
      userHasAuthenticated: this.userHasAuthenticated
    }
    return (
      !this.state.isAuthenticating && (
        <MuiThemeProvider theme={theme}>
          <Router>
            <div>
              <Route path="/" render={(props) => <Home {...props} authProps={authProps} />} />
            </div>
          </Router>
        </MuiThemeProvider>
      )
    )
  }
}

export default App
