import React, { Component } from 'react'
import LandingPage from '../components/LandingPage'
import NavBar from '../components/NavBar'
import SectionContent from '../components/SectionContent'
import { API } from 'aws-amplify'
const content = require('../content.json')
class Home extends Component {
  constructor () {
    super()
    this.state = {
      badges: [],
      sections: []
    }
  }
  async componentDidMount () {
    await this.handleBadges()
  }
  handleBadges = async () => {
    try {
      const badges = await this.getBadges()
      this.setState({ badges })
    } catch (e) {
      console.error(e)
    }
  }
  getBadges () {
    return API.get('badges', '/badges')
  }
  render () {
    const authProps = this.props.authProps
    return (
      <div>
        <NavBar authProps={authProps} handleBadges={this.handleBadges} />
        <LandingPage authProps={authProps} badges={this.state.badges} handleBadges={this.handleBadges} />
        <div>
          {content.map((section) => {
            return <SectionContent key={section.sectionName} section={section} />
          })}
        </div>
        <div />
      </div>
    )
  }
}

export default Home
