import React, { Component } from 'react'
import Modal from '@material-ui/core/Modal'
import classNames from 'classnames'
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import { withStyles } from '@material-ui/core/styles'
import ErrorIcon from '@material-ui/icons/Error'
import CheckCircleIcon from '@material-ui/icons/CheckCircle'
import SignIn from './SignIn'
// Amplify
import { Auth } from 'aws-amplify'
// CSS
// import "./LoginModal.css";
const variantIcon = {
  success: CheckCircleIcon,
  error: ErrorIcon
}
const styles = (theme) => ({
  success: {
    backgroundColor: theme.palette.primary.main
  },
  error: {
    backgroundColor: theme.palette.error.main
  },
  icon: {
    fontSize: 20
  },
  iconVariant: {
    opacity: 0.9,
    marginRight: theme.spacing.unit
  },
  message: {
    display: 'flex',
    alignItems: 'center'
  }
})
class LoginModal extends Component {
  constructor (props) {
    super(props)
    this.state = {
      username: '',
      password: '',
      isLoading: false,
      snackbarstatus: false,
      messagesnackbar: '',
      authstatus: 'success'
    }
  }
  handleModal = (event) => {
    this.props.onModalToggle(false)
  }
  handleChange = (event) => {
    this.setState({
      [event.target.id]: event.target.value
    })
  }
  handleSubmit = async (event) => {
    // TODO add captcha
    event.preventDefault()
    this.setState({ isLoading: true })
    try {
      await Auth.signIn(this.state.username, this.state.password)
      this.props.authProps.userHasAuthenticated(true)
      this.handleModalClose()
      this.setState({
        authstatus: 'success',
        snackbarstatus: true,
        messagesnackbar: 'Logged in',
        isLoading: false
      })
    } catch (err) {
      console.error(err)
      this.setState({
        authstatus: 'error',
        snackbarstatus: true,
        messagesnackbar: err.message,
        isLoading: false
      })
    }
  }
  validateForm = (event) => {
    if (this.state.password.length > 1 && this.state.username.length > 1) {
      return false
    } else {
      return true
    }
  }
  handleSnackbar = () => {
    this.setState({ snackbarstatus: false })
  }
  handleModalClose = () => {
    this.setState({
      username: '',
      password: '',
      snackbarstatus: false
    })
    this.handleModal(false)
  }
  render () {
    const { classes, className } = this.props
    const AuthIcon = variantIcon[this.state.authstatus]

    return (
      <div>
        <Modal
          id="LoginModal"
          open={this.props.openModal}
          onClose={() => {
            this.handleModalClose()
          }}
        >
          <SignIn
            handleChange={this.handleChange}
            handleSubmit={this.handleSubmit}
            errorHandler={this.state.snackbarstatus}
            formValidator={this.validateForm}
            isLoading={this.state.isLoading}
          />
        </Modal>
        <Snackbar
          anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
          open={this.state.snackbarstatus}
          onClose={this.handleSnackbar}
          autoHideDuration={4000}
          ContentProps={{
            'aria-describedby': 'message-id'
          }}
        >
          <SnackbarContent
            className={classNames(classes[this.state.authstatus], className)}
            onClose={this.handleSnackbar}
            message={
              <span id="auth-snackbar" className={classes.message}>
                <AuthIcon className={(classes.icon, classes.iconVariant)} />
                {this.state.messagesnackbar}
              </span>
            }
          />
        </Snackbar>
      </div>
    )
  }
}

export default withStyles(styles)(LoginModal)
