import React, { Component } from 'react'
import Drawer from '@material-ui/core/Drawer'
import Button from '@material-ui/core/Button'
import List from '@material-ui/core/List'
import Divider from '@material-ui/core/Divider'
import ListSubheader from '@material-ui/core/ListSubheader'
import ListItem from '@material-ui/core/ListItem'
import ListItemIcon from '@material-ui/core/ListItemIcon'
import ListItemText from '@material-ui/core/ListItemText'
import { Auth } from 'aws-amplify'
// ICON
import MenuIcon from '@material-ui/icons/Menu'
import HomeIcon from '@material-ui/icons/Home'
import ExitIcon from '@material-ui/icons/ExitToApp'
import AccountIcon from '@material-ui/icons/AccountCircle'
import AddIcon from '@material-ui/icons/Add'

import './NavBar.css'

import LoginModal from './LoginModal'
import ModalBadge from './ModalBadge'

class NavBar extends Component {
  constructor (props) {
    super(props)
    this.state = {
      drawer: false,
      showLoginModal: false,
      showBadgeModal: false
    }
    this.toggleLoginModal = this.toggleLoginModal.bind(this)
    this.toggleBadgeModal = this.toggleBadgeModal.bind(this)
  }
  toggleDrawer = (open) => () => {
    this.setState({
      drawer: open
    })
  }
  toggleLoginModal (openModal) {
    this.setState({ showLoginModal: openModal })
  }
  toggleBadgeModal (open) {
    this.setState({ showBadgeModal: open })
  }
  handleLogoutClick = () => {
    Auth.signOut()
      .then((data) => {
        this.props.authProps.userHasAuthenticated(false)
      })
      .catch((err) => console.log(err))
  }
  handleAddBadgeClick = () => {
    this.toggleBadgeModal(true)
  }
  render () {
    const authProps = {
      isAuthenticated: this.props.authProps.isAuthenticated,
      userHasAuthenticated: this.props.authProps.userHasAuthenticated
    }
    return (
      <div className="navBarButtonPosition ">
        <Button onClick={this.toggleDrawer(true)} variant="fab" color="primary">
          <MenuIcon />
        </Button>
        <Drawer open={this.state.drawer} onClose={this.toggleDrawer(false)}>
          <div tabIndex={0} role="button" onClick={this.toggleDrawer(false)} onKeyDown={this.toggleDrawer(false)}>
            <div className="navbarStyle">
              <List component="nav" subheader={<ListSubheader component="div">Esplanade</ListSubheader>}>
                <ListItem button>
                  <ListItemIcon>
                    <HomeIcon />
                  </ListItemIcon>
                  <ListItemText primary="Home" primaryTypographyProps={{ color: 'textSecondary' }} />
                </ListItem>
                {/* <ListItem button>
                  <ListItemIcon>
                    <SearchIcon />
                  </ListItemIcon>
                  <ListItemText primary="Search" />
                </ListItem> */}
              </List>
              <Divider />
              {authProps.isAuthenticated ? (
                <List component="nav">
                  <ListItem
                    button
                    onClick={() => {
                      this.handleAddBadgeClick()
                    }}
                  >
                    <ListItemIcon>
                      <AddIcon />
                    </ListItemIcon>
                    <ListItemText primary="Add badge" primaryTypographyProps={{ color: 'textSecondary' }} />
                  </ListItem>
                  <Divider />
                  <ListItem
                    button
                    onClick={() => {
                      this.handleLogoutClick()
                    }}
                  >
                    <ListItemIcon>
                      <ExitIcon />
                    </ListItemIcon>
                    <ListItemText primary="Log out" primaryTypographyProps={{ color: 'textSecondary' }} />
                  </ListItem>
                </List>
              ) : (
                <List component="nav">
                  <ListItem
                    button
                    onClick={() => {
                      this.toggleLoginModal(true)
                    }}
                  >
                    <ListItemIcon>
                      <AccountIcon />
                    </ListItemIcon>
                    <ListItemText primary="Log in" primaryTypographyProps={{ color: 'textSecondary' }} />
                  </ListItem>
                </List>
              )}
            </div>
          </div>
        </Drawer>
        <LoginModal openModal={this.state.showLoginModal} onModalToggle={this.toggleLoginModal} authProps={authProps} />
        <ModalBadge
          openModal={this.state.showBadgeModal}
          onModalToggle={this.toggleBadgeModal}
          handleBadges={this.props.handleBadges}
        />
      </div>
    )
  }
}

export default NavBar
