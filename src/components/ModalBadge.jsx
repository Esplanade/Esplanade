import React, { Component } from 'react'
import Modal from '@material-ui/core/Modal'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import CssBaseline from '@material-ui/core/CssBaseline'
import PropTypes from 'prop-types'
import Paper from '@material-ui/core/Paper'
import Avatar from '@material-ui/core/Avatar'
import NoteAdd from '@material-ui/icons/NoteAdd'
import TextField from '@material-ui/core/TextField'
import FormControl from '@material-ui/core/FormControl'
import classNames from 'classnames'
import Icon from '@material-ui/core/Icon'
import InputAdornment from '@material-ui/core/InputAdornment'
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import { API } from 'aws-amplify'

const styles = (theme) => ({
  layout: {
    width: 'auto',
    display: 'block', // Fix IE 11 issue.
    marginLeft: theme.spacing.unit * 3,
    marginRight: theme.spacing.unit * 3,
    [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
      width: 600,
      marginLeft: 'auto',
      marginRight: 'auto'
    }
  },
  paper: {
    marginTop: theme.spacing.unit * 8,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    flexWrap: 'wrap',
    padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`
  },
  avatar: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: '100%', // Fix IE11 issue.
    marginTop: theme.spacing.unit
  },
  submit: {
    marginTop: theme.spacing.unit * 3
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  margin: {
    margin: theme.spacing.unit
  },
  icon: {
    margin: theme.spacing.unit * 2
  }
})
const icons = [
  {
    value: 'fab fa-github',
    label: 'Github'
  },
  {
    value: 'fab fa-gitlab',
    label: 'Gitlab'
  },
  {
    value: 'fab fa-twitter',
    label: 'Twitter'
  },
  {
    value: 'fab fa-linkedin',
    label: 'Linkedin'
  }
]
class ModBadge extends Component {
  constructor (props) {
    super(props)
    this.state = {
      url: '',
      icon: 'fab fa-github'
    }
  }
  handleModal = (event) => {
    this.props.onModalToggle(event)
  }
  handleChange = (name) => (event) => {
    this.setState({
      [name]: event.target.value
    })
  }
  handleSubmit = async (event) => {
    event.preventDefault()
    let data = {
      link: this.state.url,
      icon: this.state.icon
    }
    await this.createBadge(data)
    this.handleModal(false)
    this.props.handleBadges()
    try {
    } catch (e) {
      console.log(e)
      alert(e)
    }
  }

  createBadge (badge) {
    return API.post('badges', '/badges', { body: badge })
  }
  formValidator = (event) => {
    if (this.state.url.length > 1 && this.state.icon.length > 1) {
      return false
    } else {
      return true
    }
  }

  render () {
    const { classes } = this.props
    return (
      <div>
        <CssBaseline />
        <Modal
          id="BadgeModal"
          open={this.props.openModal}
          onClose={() => {
            this.handleModal(false)
          }}
        >
          <main className={classes.layout}>
            <Paper className={classes.paper}>
              <Avatar className={classes.avatar}>
                <NoteAdd />
              </Avatar>
              <Typography variant="h5">Add badge</Typography>
              <form className={classes.form} noValidate autoComplete="off">
                <FormControl margin="normal" required fullWidth>
                  <TextField
                    required
                    id="filled-required"
                    label="Url"
                    className={classes.textField}
                    autoFocus
                    onChange={this.handleChange('url')}
                  />
                  <TextField
                    id="filled-select-currency-native"
                    label="Select icon"
                    select
                    className={classes.margin}
                    value={this.state.icon}
                    onChange={this.handleChange('icon')}
                    SelectProps={{
                      native: true,
                      MenuProps: {
                        className: classes.menu
                      }
                    }}
                    helperText="Please select your icon"
                    margin="normal"
                    InputProps={{
                      startAdornment: (
                        <InputAdornment position="start">
                          <Icon className={classNames(classes.icon, this.state.icon)} />{' '}
                        </InputAdornment>
                      )
                    }}
                  >
                    {icons.map((option) => (
                      <option key={option.value} value={option.value}>
                        {option.label}
                      </option>
                    ))}
                  </TextField>
                  <Button
                    type="submit"
                    fullWidth
                    variant="contained"
                    color="primary"
                    className={classes.submit}
                    onClick={this.handleSubmit}
                    disabled={this.formValidator()}
                  >
                    {this.state.isLoading ? <CircularProgress size={20} /> : 'Add'}
                  </Button>
                </FormControl>
              </form>
            </Paper>
          </main>
        </Modal>
      </div>
    )
  }
}
ModBadge.propTypes = {
  classes: PropTypes.object.isRequired
}

// We need an intermediary variable for handling the recursive nesting.
const ModalBadge = withStyles(styles)(ModBadge)

export default ModalBadge
