import React, { Component } from 'react'
import PropTypes from 'prop-types'
import classNames from 'classnames'
import { withStyles } from '@material-ui/core/styles'
import Icon from '@material-ui/core/Icon'
import DeleteIcon from '@material-ui/icons/DeleteForever'
import { API } from 'aws-amplify'

const styles = (theme) => ({
  root: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'flex-end'
  },
  icon: {
    margin: theme.spacing.unit * 2
  },
  iconHover: {
    '&:hover': {
      color: '#FFF'
    }
  },
  badge: {
    position: 'relative',
    right: '10px',
    bottom: '60px'
  }
})

class BadgeLink extends Component {
  handleBadgeDelete = () => {
    try {
      API.del('badges', `/badges/${this.props.id}`)
      this.props.handleBadges()
    } catch (e) {
      console.log(e)
    }
  }
  render () {
    const { classes } = this.props
    return (
      <span>
        <a href={this.props.link} target="_blank">
          <Icon className={classNames(classes.icon, classes.iconHover, this.props.icon)} style={{ fontSize: 60 }} />
        </a>
        {this.props.authProps.isAuthenticated && (
          <DeleteIcon className={classNames(classes.badge)} onClick={this.handleBadgeDelete} />
        )}
      </span>
    )
  }
}
BadgeLink.propTypes = {
  icon: PropTypes.string,
  link: PropTypes.string,
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(BadgeLink)
