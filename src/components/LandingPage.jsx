import React, { Component } from "react";
import { withStyles } from '@material-ui/core/styles'
import BadgeLink from './BadgeLink'
import CircularProgress from '@material-ui/core/CircularProgress'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'

import './LandingPage.css'
const styles = (theme) => ({
  title: {
    marginBottom: theme.spacing.unit * 4
  },
  LandingPage: {}
})

class LandingPage extends Component {
  constructor() {
    super()

    this.state = {
      windowHeight: 0,
      isLoading: true
    }
  }
  handleResize() {
    this.setState({
      windowHeight: window.innerHeight
    })
  }
  async componentDidMount() {
    this.handleResize()
    window.addEventListener('resize', () => this.handleResize())
    this.setState({ isLoading: false })
  }

  componentWillUnmount() {
    window.removeEventListener('resize', () => this.handleResize())
  }

  render() {
    const { classes } = this.props
    const sectionStyle = {
      minHeight: this.state.windowHeight
    }
    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
        className="LandingPageBackground"
        style={sectionStyle}
      >
        <Grid item>
          <Typography variant="h2" className={classes.title}>
            Benjamin Sanvoisin
          </Typography>
        </Grid>
        <Grid item>
          <Typography variant="h4">Hello I'm a SRE and blockchain enthusiast </Typography>
        </Grid>
        <Grid item>
          {this.state.isLoading && this.props.badges ? (
            <CircularProgress size={40} />
          ) : (
              this.props.badges.map((badge) => {
                return (
                  <BadgeLink
                    key={badge.badgeId}
                    id={badge.badgeId}
                    icon={badge.icon}
                    link={badge.link}
                    authProps={this.props.authProps}
                    handleBadges={this.props.handleBadges}
                  />
                )
              })
            )}
        </Grid>
      </Grid>
    )
  }
}

export default withStyles(styles)(LandingPage)
