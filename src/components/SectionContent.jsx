import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles'
import Typography from '@material-ui/core/Typography'
import Grid from '@material-ui/core/Grid'
import Chip from '@material-ui/core/Chip'
import Icon from '@material-ui/core/Icon'
import classNames from 'classnames'
import Divider from '@material-ui/core/Divider'

const styles = (theme) => ({
  heading: {
    margin: theme.spacing.unit * 3
  },
  section: {
    backgroundColor: theme.palette.primary.main
  },
  footer: {
    marginBottom: theme.spacing.unit * 2
  },
  content: {
    backgroundColor: '#FFF',
    paddingBottom: theme.spacing.unit * 4,
    paddingTop: theme.spacing.unit * 2
  },
  subcontent: {
    marginBottom: theme.spacing.unit * 2
  },
  chips: {
    marginBottom: theme.spacing.unit * 4
  },
  chip: {
    margin: theme.spacing.unit,
    backgroundColor: theme.palette.secondary.main
  },
  links: {
    marginLeft: theme.spacing.unit * 2
  },
  icon: {
    position: 'relative',
    top: '8px',
    marginBottom: theme.spacing.unit * 2
  },
  iconHover: {
    '&:hover': {
      color: theme.palette.primary.main
    }
  }
})

class SectionContent extends Component {
  constructor () {
    super()
    this.state = {
      windowHeight: 0
    }
  }
  handleResize () {
    this.setState({
      windowHeight: window.innerHeight
    })
  }
  async componentDidMount () {
    this.handleResize()
    window.addEventListener('resize', () => this.handleResize())
    this.setState({ isLoading: false })
  }

  componentWillUnmount () {
    window.removeEventListener('resize', () => this.handleResize())
  }
  render () {
    const { classes } = this.props

    return (
      <div>
        <Grid container direction="row" justify="center" alignItems="center">
          <Grid container item xs={12} className={classes.section} justify="center" alignItems="center">
            <Grid item xs={10}>
              <Typography variant="h3" className={classes.heading}>
                {this.props.section.sectionName}
              </Typography>
            </Grid>
          </Grid>
          {/* Custom content - description, links & chips */}
          {this.props.section.content.map((content) => {
            return (
              <div key={content.description}>
                <Grid
                  container
                  direction="row"
                  justify="space-evenly"
                  alignItems="flex-start"
                  className={classes.content}
                >
                  <Grid
                    container
                    item
                    xs={11}
                    direction="column"
                    justify="center"
                    alignItems="center"
                    className={classes.subcontent}
                  >
                    <Grid item xs={12}>
                      <Typography variant="h4" color="textSecondary">
                        {content.description}
                      </Typography>
                    </Grid>
                    <Grid item xs={12}>
                      {content.urls.map((link) => {
                        return (
                          <a href={link.url} key={link.url} target="_blank" className={classes.links}>
                            <Icon className={classNames(classes.icon, classes.iconHover, link.icon)} />
                          </a>
                        )
                      })}
                    </Grid>
                  </Grid>
                  <Grid item container direction="row" justify="space-evenly" alignItems="center">
                    <Grid
                      container
                      item
                      xs={11}
                      md={5}
                      className={classes.chips}
                      direction="row"
                      justify="space-evenly"
                      alignItems="center"
                    >
                      {content.chips.map((chip) => {
                        return (
                          <Grid key={chip}>
                            <Chip label={chip} color="secondary" className={classes.chip} />
                          </Grid>
                        )
                      })}
                    </Grid>
                    <Grid item xs={11} md={5}>
                      <Typography variant="body1" color="textSecondary">
                        {content.summary}
                      </Typography>
                    </Grid>
                  </Grid>
                </Grid>
                <Divider />
              </div>
            )
          })}
        </Grid>
      </div>
    )
  }
}
export default withStyles(styles)(SectionContent)
